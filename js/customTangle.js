//----------------------------------------------------------
//
//  TKCircle
//
//  Circle through variables

Tangle.classes.TKCircle = {

    initialize: function (element, options, tangle, variable, variable2) {
        element.addEvent("click", function (event) {
            var index = parseInt(tangle.getValue(variable2));
            if (index < db[options.category].length-1) {
                index = index+1;
                var instance = db[options.category][index].nome;
                tangle.setValue(variable2, index);
                tangle.setValue(variable, instance);
            }
            else {
                index = index-1;
                var instance = db[options.category][index].nome;
                tangle.setValue(variable2, index);
                tangle.setValue(variable, instance);
            }
        });
    },
};

//CUSTOM
